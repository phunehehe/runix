#!/usr/bin/env bash
set -efuo pipefail
my_dir=$(readlink -f "$(dirname "$0")")
nix-build --no-out-link "$my_dir/example/runix/default.nix"
