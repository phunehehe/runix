let

  inherit (builtins) head;

  lib = import <nixpkgs/lib>;
  inherit (lib)
    attrValues concatMapStringsSep concatStrings filterAttrs getAttrFromPath
    hasAttrByPath mapAttrsToList;

  pkgs = import <nixpkgs> {};


  writeCheckedBash = { name, script, destination ? "" }:
    pkgs.runCommand name { inherit script; } ''
      f=$out${destination}
      mkdir --parents $(dirname $f)
      {
        echo '#!${pkgs.bash}/bin/bash'
        echo 'set -efuo pipefail'
        echo "$script"
      } > $f
      chmod +x $f
      ${pkgs.haskellPackages.ShellCheck}/bin/shellcheck $f
    '';

  writeBashBin = name: script: writeCheckedBash {
    inherit name script;
    destination = "/bin/${name}";
  };

  writeBash = name: script: "${writeBashBin name script}/bin/${name}";


  svlogd-config = pkgs.writeText "config" ''
    !${pkgs.gzip}/bin/gzip
  '';

  svlogd-run = dir: writeBash "svlogd-logger" ''
    ${pkgs.coreutils}/bin/mkdir --parents ${dir}
    ${pkgs.coreutils}/bin/ln --force --symbolic ${svlogd-config} ${dir}/config
    exec ${pkgs.runit}/bin/svlogd -tt ${dir}
  '';


  here = pkgs // rec {

    inherit writeBash writeBashBin;

    translateSignal = writeBash "translate-signal" ''
      from_signal=$1
      shift
      to_signal=$1
      shift
      "$@" &
      pid=$!
      translate() {
        kill -s "$to_signal" $pid
      }
      trap translate "$from_signal"
      wait
    '';

    collectAttrByPath = attrPath: e:
      map (value: getAttrFromPath attrPath value)
        (attrValues (filterAttrs (_: value: hasAttrByPath attrPath value) e));


    buildRunitDir = baseLogDir: processes:
    let
      runitDir = pkgs.runCommand "runit-dir" {} (concatStrings (mapAttrsToList (
        name: command:
          let
            run = writeBash name ''
              exec 2>&1 ${command}
            '';
          in ''
            mkdir -p $out/${name}/log
            ln -s ${svlogd-run "${baseLogDir}/${name}/svlogd"} "$out/${name}/log/run"
            ln -s ${run} $out/${name}/run
          ''
      ) processes));
      store = "/etc/runit/runsvdir/store/${baseNameOf runitDir}";
    in writeBash "runit-dir" ''
      ${pkgs.coreutils}/bin/mkdir --parents ${store}
      ${pkgs.coreutils}/bin/cp --no-target-directory --recursive ${runitDir} ${store}
      echo ${store}
    '';

    runsvdir = baseLogDir: processes: writeBash "runsvdir" ''

      # runsvdir needs to find runsv
      export PATH=${lib.makeBinPath [
        pkgs.runit
      ]}

      store=$(${buildRunitDir baseLogDir processes})
      exec ${translateSignal} TERM HUP runsvdir "$store"
    '';
  };

in here // {
  leUtils = import ./le-utils.nix { inherit lib; pkgs = here; };
}
