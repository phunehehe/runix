{ lib, pkgs }:

let

  # These are dictated by simp_le
  cert = "fullchain.pem";
  key = "key.pem";

in {

  renewHooks = exports: domain:
    pkgs.collectAttrByPath ["letsencrypt" "renew-hooks" domain] exports;


  nginxWellKnown = dir: ''
    location ~ /.well-known {
      root ${dir};
    }
  '';

  nginxSslPaths = dir: ''
    ssl_certificate      ${dir}/${cert};
    ssl_certificate_key  ${dir}/${key};
  '';


  renew = domains: dir: hooks:
  let hook = pkgs.writeBash "hook.sh" (lib.concatStrings hooks);
  in pkgs.writeBash "renew-${builtins.head domains}.sh" ''

    PATH=${lib.makeBinPath [
      pkgs.coreutils
    ]}

    mkdir -p ${dir}
    cd ${dir}

    ${pkgs.simp_le}/bin/simp_le \
      ${lib.concatMapStringsSep " " (d: "-d ${d}") domains} \
      -f account_key.json \
      -f account_reg.json \
      -f ${cert} \
      -f ${key} \
      --default_root . \
      && ${hook}

    chmod go-rwx ${key}
    exec sleep $(( 60 * 60 * 24 ))
  '';

  selfSignCert = dir:
  let certPath = "${dir}/${cert}";
  in pkgs.writeBash "self-sign.sh" ''
    ${pkgs.coreutils}/bin/mkdir --parents ${dir}
    [[ -e ${certPath} ]] || ${pkgs.openssl}/bin/openssl req -x509 -nodes -batch \
      -newkey rsa \
      -out ${certPath} \
      -keyout ${dir}/${key}
  '';
}
