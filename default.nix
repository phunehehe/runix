with builtins;

let

  lib = import <nixpkgs/lib>;
  pkgs = import ./pkgs;
  baseDir = "/srv";

  inherit (lib)
    evalModules filterAttrs genAttrs isDerivation optionalAttrs;
  inherit (pkgs) buildRunitDir writeBashBin;

  read-app = name: config: exports:
  let
    home = "${baseDir}/${name}";
    spec = import (../apps + "/${name}") {
      inherit home exports lib pkgs;
      config = config.${name};
    };
    run = if isAttrs spec && ! isDerivation spec
      then spec.run else spec;
  in {
    inherit run;
    exports = optionalAttrs (isAttrs spec) (spec.exports or {});
    options = optionalAttrs (isAttrs spec) (spec.options or {});
  };

  app-names = attrNames (filterAttrs (p: _:
    pathExists (../apps + "/${p}/default.nix")
  ) (readDir ../apps));

  options = genAttrs app-names (name: (read-app name {} {}).options);

  inherit (evalModules {
    modules = [
      { inherit options; }
      ../config.nix
    ];
  }) config;

  # Can't use exports when you are making exports
  exports = genAttrs app-names (name: (read-app name config {}).exports);

  processes = genAttrs app-names (name: (read-app name config exports).run);


  rootRunitDir = "/etc/runit/runsvdir/current";

  rebuild = pkgs.writeBash "rebuild" ''

    PATH=${pkgs.coreutils}/bin

    # Else runsvchdir will complain on the first rebuild
    mkdir --parents "$(dirname ${rootRunitDir})"
    touch ${rootRunitDir}

    store=$(${buildRunitDir baseDir processes})
    exec ${pkgs.runit}/bin/runsvchdir "$store"
  '';

  runit = pkgs.writeBash "runit" ''

    ${rebuild}

    # runsvdir needs to find runsv
    export PATH=${lib.makeBinPath [
      pkgs.runit
    ]}

    exec ${pkgs.translateSignal} TERM HUP runsvdir ${rootRunitDir}
  '';

  safeRebuild = pkgs.writeBashBin "rebuild" ''
    exec -c ${rebuild}
  '';

  safeRunit = pkgs.writeBashBin "runit" ''
    exec -c ${runit}
  '';


in pkgs.buildEnv {
  name = "runix";
  paths = [
    safeRebuild
    safeRunit
  ];
}
