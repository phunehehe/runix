{ exports, pkgs, ... }:
pkgs.writeBash "motd" ''
  while true
  do
    echo MOTD is ${exports.hello.greeting}
    sleep 1
  done
''
