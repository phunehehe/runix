{ config, lib, pkgs, ... }:
let
  greeting = "Hello, ${config.name}!";
in {
  options.name = lib.mkOption {
    type = lib.types.str;
  };
  exports = {
    inherit greeting;
  };
  run = pkgs.writeBash "hello" ''
    while true
    do
      echo ${greeting}
      sleep 1
    done
  '';
}
