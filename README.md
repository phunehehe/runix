# Runix = Runit + Nix

[![build status](https://gitlab.com/phunehehe/runix/badges/master/build.svg)](https://gitlab.com/phunehehe/runix/commits/master)

Runix is a configuration management tool like [Ansible](http://www.ansible.com/)
and [Puppet](http://www.puppetlabs.com/), built with the ultimate goal of
running services.

It turns out, "running services" covers a lot of grounds. Web applications and
databases are services. Probably anything that has the word "service" in it is
a service. Almost by definition, all servers do is run services :P

Here are some less trivial cases:

  - One-shot "services" may not be services, depending on your definition, but
    if you create a program that does something and then `sleep infinity`, it
    can probably be called a service. Runix might also grow a less hacky
    mechanism in the future should the need appears.
  - Cron jobs can be implemented as services. They can also be implemented with
    a one-shot service that drops files into `/etc/cron.d`. See
    [Unicron](https://github.com/MichaelXavier/cron/pull/21) for another
    example.
  - Ad-hoc tools that are meant to be run manually by humans do not fit well
    into this model. They may not be the best way to do things though. Automate
    harder! Or use a hack like a one-shot service that drops the right file
    into the right place.

Internally, Runix uses the word "app" instead of "service" to signify that it
provides more than just traditional services.

# Features

Runix provides all the features of [Runit](http://smarden.org/runit/) and
[Nix](http://nixos.org/nix/), notably:

  - Runix is distribution agnostic.

If you run NixOS, you probably don't need Runix :) But if you want to avoid the
radical path of converting all your servers to NixOS, this is for you. There is
no need to worry about clashing with existing files or packages either.

Unless you are already using Runit and/or Nix, but then you are already a pro at
this. Let me know how things go ;)

  - An app's dependencies are safely isolated from other apps' dependencies.

That is, you can use a version of a library in one app, and use another
incompatible version of the same library in another app. They will happily live
together.

That said, it's recommended to use the same package set among your apps to avoid
space (and maintenance) overhead.

  - Upgrades and rollbacks are easy and atomic

This is probably best explained by quoting the [Nix
manual](http://nixos.org/nix/manual/#idm46912468159056):

> Since package management operations never overwrite packages in the Nix store
but just add new versions in different paths, they are atomic.

> And since package aren’t overwritten, the old versions are still there after
an upgrade.

# Apps

Here are some available apps:

  - [![build status](https://gitlab.com/phunehehe/runix-archiveopteryx/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-archiveopteryx/commits/master) [archiveopteryx](https://gitlab.com/phunehehe/runix-archiveopteryx)
  - [![build status](https://gitlab.com/phunehehe/runix-attic/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-attic/commits/master) [attic](https://gitlab.com/phunehehe/runix-attic)
  - [![build status](https://gitlab.com/phunehehe/runix-docker-daemon/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-docker-daemon/commits/master) [docker-daemon](https://gitlab.com/phunehehe/runix-docker-daemon)
  - [![build status](https://gitlab.com/phunehehe/runix-foomail/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-foomail/commits/master) [foomail](https://gitlab.com/phunehehe/runix-foomail)
  - [![build status](https://gitlab.com/phunehehe/runix-gitlab-runner/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-gitlab-runner/commits/master) [gitlab-runner](https://gitlab.com/phunehehe/runix-gitlab-runner)
  - [![build status](https://gitlab.com/phunehehe/runix-nginx/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-nginx/commits/master) [nginx](https://gitlab.com/phunehehe/runix-nginx)
  - [![build status](https://gitlab.com/phunehehe/runix-nix-daemon/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-nix-daemon/commits/master) [nix-daemon](https://gitlab.com/phunehehe/runix-nix-daemon)
  - [![build status](https://gitlab.com/phunehehe/runix-opendkim/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-opendkim/commits/master) [opendkim](https://gitlab.com/phunehehe/runix-opendkim)
  - [![build status](https://gitlab.com/phunehehe/runix-php-fpm/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-php-fpm/commits/master) [php-fpm](https://gitlab.com/phunehehe/runix-php-fpm)
  - [![build status](https://gitlab.com/phunehehe/runix-postfix/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-postfix/commits/master) [postfix](https://gitlab.com/phunehehe/runix-postfix)
  - [![build status](https://gitlab.com/phunehehe/runix-postgresql/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-postgresql/commits/master) [postgresql](https://gitlab.com/phunehehe/runix-postgresql)
  - [![build status](https://gitlab.com/phunehehe/runix-rainloop/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-rainloop/commits/master) [rainloop](https://gitlab.com/phunehehe/runix-rainloop)
  - [![build status](https://gitlab.com/phunehehe/runix-roundcube/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-roundcube/commits/master) [roundcube](https://gitlab.com/phunehehe/runix-roundcube)

## Making an app

A Runix app is an attribute set containing these keys:

  - `run` is the only required attribute. This can be a command (string) or an
    executable, which will be turned into a Runit service.
  - `options` defines options that can be customized in the project's config
    file. The values of these options will be passed back to the app via the
    argument `config`.
  - `exports` are things that this app wants to share with other apps. This
    allows for inter-app intergrations. Runix does not try to control exports.
    It is up to apps to define and maintain this interface among themselves.
    For simplicity, an app's exports cannot use other apps' exports.

Any output (`stdout` and `stderr`) of the app's process is captured and managed
by [`svlogd`](http://smarden.org/runit/svlogd.8.html). There is no need for a
dedicated logging and rotating mechanism unless you want to go out of your way
to do so.

Have a look at [hello](example/apps/hello/default.nix) to see an example app.

In the simplest case, if you don't need to define `options` or `exports`,
skipping the entire attribute set leaving only the value of `run` works too.

Have a look at [motd](example/apps/motd/default.nix) to see another example app
with this short-hand syntax and a simple use of `exports`.

# Using Runix

Let's try the [example](example)!

## Initial setup

  - [Install Nix](http://nixos.org/nix/manual/#chap-installation) on your
    development and target machines

```bash
# Yeah I'm not fond of `curl-bash`ing either
bash <(curl https://nixos.org/nix/install)
```

  - Build and deploy

```bash
cd example
target=some-machine # Replace with yours of course
result=$(nix-build runix)
nix-copy-closure --to $target $result
ssh $target "nix-env --profile /nix/var/nix/profiles/runix --set $result"
```

  - On the server, under `/nix/var/nix/profiles/runix/bin` you will see 2
    (symlinks to) executables `runit` and `rebuild`. Run `runit` however you
    want. It will stay in the foreground. I use a `tmux` window :P

# Subsequent deployments

  - Just run `rebuild` after deploying

```bash
result=$(nix-build runix)
nix-copy-closure --to $target $result
ssh $target "
    nix-env --profile /nix/var/nix/profiles/runix --set $result
    /nix/var/nix/profiles/runix/bin/rebuild
"
```
